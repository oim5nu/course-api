export interface ICourse {
  id: string;
  name: string;
  type: 'chemistry' | 'math' | 'english';
}
